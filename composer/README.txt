once you have the images created, and docker-compose is installed.

cd into composer directory:
there should be two files here:
	1. docker-compose.yml
	2. .env (if it exists as parameters.env, please rename it to .env)

provide relevant parameters in .env file
docker-compose up