#!/bin/sh

while [ ! -f /scripts_for_compose/flag_tensorflow_on_input_images ]; do
    sleep 1
done

rm -f /scripts_for_compose/flag_tensorflow_on_input_images
echo "executing extracted_frames.sh"
if test "$(ls -A "/input_videos/")"; then
    cd /input_videos/
    rm -rf /extracted_frames/*
    echo ----------------------------------- emptied extracted_frames_folder --------------------------
    mkdir -p /extracted_frames/
    frame_per_sec=$1

    for f in *.mp4
    do
        #strips out filepath and file extension
        videoName=$(echo $f | sed "s/.*\///" | sed "s/\..*//")
        frameRate=$(ffmpeg -i $f 2>&1 | sed -n "s/.*, \(.*\) fp.*/\1/p")
        mkdir -p /extracted_frames/$videoName
    
    
        #    echo $videoName >> testing123.txt   -> this outputs rugby
        #    echo $f >> testing123.txt          -> this outputs rugby.mp4
    
        #extracting timeframe for frames
        rm -f *remove*
        rm -f ${videoName}_frame_timestamp.txt
        ffmpeg -i $f -vframes 1 -vf showinfo,fps=fps=1/$(($frame_per_sec*100)) ${videoName}_remove_%05d.jpg > ${videoName}_remove_output.txt 2>&1
        grep pts_time ${videoName}_remove_output.txt > ${videoName}_remove_output2.txt
        sed 's/^.*pts_time/pts_time/' ${videoName}_remove_output2.txt >> ${videoName}_remove_output3.txt
        sed 's/pos.*//' ${videoName}_remove_output3.txt >> ${videoName}_remove_output4.txt
        rm -f ${videoName}_frame_timestamp.txt
        cut -f2 -d":" ${videoName}_remove_output4.txt >> ${videoName}_frame_timestamp.txt
        #    sed -i 's/./_/g' ${videoName}_frame_timestamp.txt
        rm -f *remove*
    
        #extracting frames    
        ffmpeg -i $f /extracted_frames/$videoName/$videoName%05d.jpg -hide_banner   
    
        #timestamping each frame
        initial=1
        while read p; do
            auto_increment=$(printf "%05d" $initial)
            echo /extracted_frames/${videoName}/${videoName}${auto_increment}.jpg 
            echo /extracted_frames/${videoName}/${videoName}_${p}.jpg
            mv /extracted_frames/${videoName}/${videoName}${auto_increment}.jpg /extracted_frames/${videoName}/${videoName}_${auto_increment}_${p}.jpg
            initial=$(($initial+1))
        done <${videoName}_frame_timestamp.txt
    done
else
    echo "/input_videos/ is empty"
fi
touch /scripts_for_compose/flag_extract_frames
exit 0 
  

