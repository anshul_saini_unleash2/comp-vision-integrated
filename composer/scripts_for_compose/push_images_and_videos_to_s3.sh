#!/bin/bash

while [ ! -f /scripts_for_compose/flag_create_clip ]; do
    sleep 1
done
rm -f /scripts_for_compose/flag_create_clip

echo "executing push_images_and_videos_to_s3.sh"
userid=$1
deviceid=$2
session=$3
export AWS_ACCESS_KEY_ID=$4
export AWS_SECRET_ACCESS_KEY=$5

cd /scripts_for_compose/

for D in `find -type d -printf '%P\n'`
do
    cd $D
    /root/.local/bin/aws s3 cp . s3://pylot/$userid/$deviceid/$session/tensorflow_output/ --recursive
    cd ..
done
echo "Complete!"
exit 0
# this is where you can add a cli to update the status of the job in dynamoDB