#!/bin/bash

while [ ! -f /scripts_for_compose/flag_extract_frames ]; do
    sleep 1
done

rm -f /scripts_for_compose/flag_extract_frames
echo "executing run_tensorflow_on_extracted_frames.sh"
cd /images_to_run_tensorflow_on/

#for D in `find . -type d`
for D in `find -type d -printf '%P\n'`
do
    rm -f /scripts_for_compose/${D}_tensorflow_output.txt    
    cd ./$D
    for f in *.jpg
    do
        python /scripts_for_compose/label_image.py $f >> /scripts_for_compose/${D}_tensorflow_output.txt 2> /dev/null #2> /dev/null redirects stderr to nothing
    done
          
    threshold=0.8
    while IFS=_ read aa bb cc dd ee;do
        file=$aa #for e.g. rugby
        frameNumber=$bb #for e.g. 0001
        timeStamp=$cc #for e.g. 1.2 (for 10fps)
        category=$dd #for e.g. kickoff
        probability=$ee #for e.g. 0.82345
        
        if [ $(echo "$probability > $threshold" | bc -l) -eq 1 ]
        then
            #rm -rf /scripts_for_compose/${category}/
            mkdir -p /scripts_for_compose/${category}/
            cp ${file}_${frameNumber}_${timeStamp}.jpg /scripts_for_compose/${category}/
        fi                
    done </scripts_for_compose/${D}_tensorflow_output.txt
done
touch /scripts_for_compose/flag_run_tensorflow_on_extracted_frames
exit 0
