#!/bin/bash

while [ ! -f /scripts_for_compose/flag_fetch_images_and_videos ]; do
    sleep 1
done

rm -f /scripts_for_compose/flag_fetch_images_and_videos
echo "executing run_tensorflow_on_input_images.sh"
if test "$(ls -A "/input_images/")"; then
    cd /input_images/
    rm -rf /scripts_for_compose/input_images_categorised/
    rm -f /scripts_for_compose/input_images_tensorflow_output.txt
    for f in *.jpg
    do
        python /scripts_for_compose/label_image.py $f >> /scripts_for_compose/input_images_tensorflow_output.txt 2> /dev/null #2> /dev/null redirects stderr to nothing
    done
          
    threshold="$1"
    while IFS=_ read aa bb cc;do
        file=$aa #for e.g. rugby01
        category=$bb #for e.g. kickoff
        probability=$cc #for e.g. 0.82345
        #if (( $(echo "$probability > $threshold" | bc -l) ))
        if [ $(echo "$probability > $threshold" | bc -l) -eq 1 ]
        then
            mkdir -p /scripts_for_compose/input_images_categorised/${category}/
            cp ${aa}.jpg /scripts_for_compose/input_images_categorised/${category}/
        fi                
    done < /scripts_for_compose/input_images_tensorflow_output.txt
    mv /scripts_for_compose/input_images_tensorflow_output.txt /scripts_for_compose/input_images_categorised/input_images_tensorflow_output.txt
else
    echo "/input_images/ is empty"
fi                   
touch /scripts_for_compose/flag_tensorflow_on_input_images
exit 0

