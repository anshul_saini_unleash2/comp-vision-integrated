#!/bin/bash
echo "executing fetch_images_and_videos.sh"
input_string=$3
input_string=${input_string::-1}
input_string=${input_string#?}
export AWS_ACCESS_KEY_ID=$1
export AWS_SECRET_ACCESS_KEY=$2

IFS='+++ ' read -r -a array <<< "$input_string"
for element in "${array[@]}"
do
    filename=$(basename "$element")
    extension="${filename##*.}"
    if [[ $extension == "jpeg" ]] || [[ $extension == "jpg" ]]
    then
        /root/.local/bin/aws s3 cp $element /input_images/$filename --quiet
    else
        /root/.local/bin/aws s3 cp $element /input_videos/$filename --quiet
    fi
done
touch /scripts_for_compose/flag_fetch_images_and_videos
exit 0
