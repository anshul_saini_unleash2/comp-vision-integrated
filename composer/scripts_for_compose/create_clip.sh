#!/bin/bash

while [ ! -f /scripts_for_compose/flag_run_tensorflow_on_extracted_frames ]; do
    sleep 1
done

rm -f /scripts_for_compose/flag_run_tensorflow_on_extracted_frames
echo "executing create_clip.sh"
cd /scripts_for_compose/

for D in `find -type d -printf '%P\n'`
do
    if [ $D != "input_images_categorised" ] || [ $D != "clips" ]
    then
        cd $D
        initial=1
   
        for f in *.jpg
        do
            IFS=_ read aa bb cc <<< "$f"
            fileName=$aa #for e.g. rugby
            frameNumber=$bb #for e.g. 0001
            timeStamp=${cc%.*} #for e.g. 1.2 (for 10fps)                
            category=$D #for e.g. kickoff
            clipSeconds=3
                    
            startTime=$(echo "$timeStamp $clipSeconds" | awk '{print $1-$2}')    
            endTime=$(echo "$timeStamp $clipSeconds" | awk '{print $1+$2}')
                                
            if (( $(echo "$startTime < 0" | bc -l) ))
            then
                startTime=0
            fi
            mkdir -p /scripts_for_compose/${category}/clips/
            auto_increment=$(printf "%05d" $initial) 
            ffmpeg -ss $startTime -i /input_videos/${fileName}.mp4 -t $(($clipSeconds * 2)) -c copy /scripts_for_compose/${category}/clips/${fileName}_${auto_increment}.mp4
            initial=$(($initial+1))      
        done
    fi
    cd ..
done
touch /scripts_for_compose/flag_create_clip
exit 0

